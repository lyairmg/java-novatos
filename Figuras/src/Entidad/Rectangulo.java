/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;

/**
 *
 * @author Yair
 */
public class Rectangulo extends Figuras {
    private double largo;
    private double ancho;
    public Rectangulo(){
        
    }
    public Rectangulo(double largo, double ancho){
        this.ancho=ancho;
        this.largo=largo;
    }

    /**
     * @return the largo
     */
    public double getLargo() {
        return largo;
    }

    /**
     * @param largo the largo to set
     */
    public void setLargo(double largo) {
        this.largo = largo;
    }

    /**
     * @return the ancho
     */
    public double getAncho() {
        return ancho;
    }

    /**
     * @param ancho the ancho to set
     */
    public void setAncho(double ancho) {
        this.ancho = ancho;
    }
    
    @Override
    public double Calculararea() {
        return ancho*largo;
    }
}