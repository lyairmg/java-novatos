/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;

/**
 *
 * @author Yair
 */
public class Circulo extends Figuras {
    private double radio;
    public Circulo(double radio){
        this.radio=radio;
    }

    /**
     * @return the radio
     */
    public double getRadio() {
        return radio;
    }

    /**
     * @param radio the radio to set
     */
    public void setRadio(double radio) {
        this.radio = radio;
    }
    @Override
    public double Calculararea() {
        return radio*radio*Math.PI;
    }
}