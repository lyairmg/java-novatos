package Recursos.xmlEscuelas;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class cargador {
   
    public static List<Item> cargarDatosDesdeXml( String rutaXML, String[] tags) throws Exception {
    	// Obtiene los datos de un fichero XML y los carga a una lista.
        // rutaXML indica la direccion del fichero XML a leer
        // tags indica los tags que seran buscados en el fichero XML.
        // tags es un arreglo de tres valores: El primer valor indica el tag padre o raiz.
        // El segundo y tercer valor del arreglo indican hojas de del tag raiz, Se debe tener
        // en consideracion que el segundo valor indica un identificador Unico y el tercer valor
        // indica una descripcion.
        // Devuelve la lista cargada con la informacion del fichero XML
        List<Item> lista = new ArrayList<>();
        try {          
        	//TAGS SON LOS TERMINOS EJEM:<PERSONAS> QUE CONTIENE EL XML ESTE SE ENCARGA DE BUSCAR DICHOS TERMINOS Y LOS ALMACENA EN UN ARREGLO
            File file = new File(rutaXML);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = (Document) db.parse(file);
            doc.getDocumentElement().normalize();
            NodeList nodeLst = doc.getElementsByTagName(tags[0]);

            for (int s = 0; s < nodeLst.getLength(); s++) {

                Node fstNode = nodeLst.item(s);

                if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
                    Item item = new Item();
                    Element fstElmnt = (Element) fstNode;
                    NodeList fstNmElmntLst = fstElmnt.getElementsByTagName(tags[1]);
                    Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
                    NodeList fstNm = fstNmElmnt.getChildNodes();
                    item.setItemData(Integer.parseInt(((Node) fstNm.item(0)).getNodeValue()));
                    NodeList lstNmElmntLst = fstElmnt.getElementsByTagName(tags[2]);
                    Element lstNmElmnt = (Element) lstNmElmntLst.item(0);
                    NodeList lstNm = lstNmElmnt.getChildNodes();
                    item.setItem(((Node) lstNm.item(0)).getNodeValue());
                    lista.add(item);
                }

            }
        } catch (IOException | NumberFormatException | ParserConfigurationException | DOMException | SAXException e) {
        }
        return lista;
    }
}
