/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;

import java.util.Date;

/**
 *
 * @author Yair
 */
public abstract class Persona {
    private String Nombre;
    private String Apellido;
    private int Lada;
    private long Telefono;
    private long fNaci;
    private String Email;
    private String Direccion;
    private String Localidad;
    
    
    public Persona(){
        
    }
    public Persona(String Nombre, String Apellido, int Lada, int Telefono, long fNaci, String Email, String Direccion, String Localidad) {
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Lada = Lada;
        this.Telefono = Telefono;
        this.fNaci = fNaci;
        this.Email = Email;
        this.Direccion = Direccion;
        this.Localidad = Localidad;
   
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre=Nombre;

    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;

    }

    public int getLada() {
        return Lada;
    }

    public void setLada(int Lada) {
        this.Lada = Lada;
    }

    public long getTelefono() {
        return Telefono;
    }

    public void setTelefono(int Telefono) {
        this.Telefono = Telefono;

    }

    public long getfNaci() {
        return fNaci;
    }

    public void setfNaci(long fNaci) {
        this.fNaci = fNaci;

    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;

    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;

    }

    public String getLocalidad() {
        return Localidad;
    }

    public void setLocalidad(String Localidad) {
        this.Localidad = Localidad;

    }

    public abstract String Registrar();
}