/*
 * Copyright 2014 Yair.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package Entidad;

import java.io.Serializable;
import java.util.Properties;

/**
 *
 * @author Yair
 */
public class Alumno extends Persona implements Serializable{
    Archivos texto = new Archivos();
    
    private long Matricula;
    private String Img;
    private String Especialidad;
    private String Notas;
    private String Escuela;
    Properties prop;
    public Alumno(){
    
    }
    
    public Alumno(long Matricula, String Especialidad, int Lada, int Telefono, String Notas, String Img, String Nombre, String Apellido, long fNaci, String Email, String Direccion, String Localidad, String Escuela) {
        super(Nombre, Apellido, Lada, Telefono, fNaci, Email, Direccion, Localidad);
        this.Matricula = Matricula;
        this.Img = Img;
        this.Especialidad=Especialidad;
        this.Notas = Notas;
        this.Escuela=Escuela;
    }
    @Override
    public String Registrar(){
        prop=new Properties();
        texto.abrirFlujo("Recursos/Contactos/"+getNombre()+" "+getApellido()+".ini");
        texto.guardarDatos("[Datos]");
        texto.guardarDatos("Nombre="+getNombre());
        texto.guardarDatos("Apellido="+getApellido());
        texto.guardarDatos("Lada="+getLada());
        texto.guardarDatos("Telefono="+getTelefono());
        texto.guardarDatos("Cumpleanos="+getfNaci());
        texto.guardarDatos("Correo="+getEmail());
        texto.guardarDatos("Direccion="+getDireccion());
        texto.guardarDatos("Localidad="+getLocalidad());
        texto.guardarDatos("Matricula="+getMatricula());
        texto.guardarDatos("Imagen="+getImg());
        texto.guardarDatos("Especialidad="+getEspecialidad());
        texto.guardarDatos("Escuela="+getEscuela());
        texto.guardarDatos("Notas="+getNotas());
        texto.guardarDatos("Desarrollado por: lyairmg");
        texto.cerrarFlujo();
        String estado="Cumplido";
        return estado;
    }
    public String getEscuela() {
        return Escuela;
    }

    public void setEscuela(String Escuela) {
        this.Escuela = Escuela;
    }

    public String getEspecialidad() {
        return Especialidad;
    }

    public void setEspecialidad(String Especialidad) {
        this.Especialidad = Especialidad;
    }

    public long getMatricula() {
        return Matricula;
    }

    public void setMatricula(long Matricula) {
        this.Matricula = Matricula;
    }

    public String getImg() {
        return Img;
    }

    public void setImg(String Img) {
        this.Img = Img;
    }
    
    public String getNotas() {
        return Notas;
    }

    public void setNotas(String Notas) {
        this.Notas = Notas;
    }
}