/*
 * Copyright 2014 Yair.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package Entidad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.swing.DefaultListModel;

/**
 *
 * @author Yair
 */
public class Archivos {
    private String nombreArchivo;
    private FileWriter escribir;
    private PrintWriter archivoPrint;
    private FileReader leerDatos;
    private File Directorio;
    private BufferedReader buferDatos;
    private StringTokenizer st;
    private Properties p;

    public Archivos() {
        nombreArchivo="";
    }
    
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }
    
    public void abrirFlujo(String nombreArchivo){
         try{
             escribir =new FileWriter(nombreArchivo);
             archivoPrint = new PrintWriter(escribir);
         }catch (FileNotFoundException e){
             System.err.println("Archivo no encontrado"+e);
         } catch (IOException ex) {
            System.err.println("No es posible usar el archivo"+ex);
        }
    }
    public void cerrarFlujo(){
        try{
            escribir.close();
            archivoPrint.close();
        }catch (IOException e){
             System.err.println("No es posible usar el archivo"+e);
         }
    }
    public void guardarDatos(String Datos){
        archivoPrint.println(Datos);   
    }
    public DefaultListModel leeDireccion(String ruta){
        Directorio = new File(ruta);
        ArrayList a=new ArrayList();
        DefaultListModel m = new DefaultListModel();
        if (Directorio.exists()){
            File[] ficheros = Directorio.listFiles();
            for (File fichero : ficheros) {
                a.add(fichero.getName().replace(".ini", ""));
            }
            for(int i=0; i<a.size(); i++) {
                //Añadir cada elemento del ArrayList en el modelo de la lista
                m.add(i, a.get(i));
            }
        }else{
            System.out.println("Agregar contacto");
        }
        return m;
    }
    
    public String aPropiedades(String Datos){
        try {
            //creamos el objeto para almacenar las propiedades
            p = new Properties();
            //cargamos propiedades, de esta forma podremos añadir y modificar las propiedades de este archivo
            p.load(new FileInputStream(Datos));
            //obtenemos una enumeracion con los nombres de las propiedades
            Enumeration propiedades=p.propertyNames();
            //hacemos un ciclo para obtener todas las propiedades
            while (propiedades.hasMoreElements()){
            String propiedad=(String)(propiedades.nextElement());
            System.out.println ( propiedad + " : " + p.getProperty(propiedad));
        }
    }catch (IOException ex){
            System.err.println(ex);
    }
        return null;
}

    public String procesarDatos(String contacto){
        try {
            leerDatos = new FileReader(new File(contacto));
            buferDatos = new BufferedReader(leerDatos);
            String line=buferDatos.readLine();
            while(line!=null){
               System.out.println(line);
               line = buferDatos.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.err.printf("Archivo no encontrado", ex);
        } catch (IOException ex) {
            System.err.printf("Error critico en el archivo",ex);
        }
        return null;
    }
}