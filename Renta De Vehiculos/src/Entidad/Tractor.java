/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;

/**
 *
 * @author Yair
 */
public class Tractor extends Vehiculo {
    private Date FRenta;
    private Date FEntrega;

    /**
     *
     * @param placas
     * @param precio
     * @param estado
     * @param FEntrega
     * @param FRenta
     */
    public Tractor(String placas, float precio, boolean estado, Date FEntrega, Date FRenta){
        super(placas, precio, estado);
        this.FEntrega=FEntrega;
        this.FRenta=FRenta;
    }

    /**
     *
     * @return
     */
    public Date getFRenta() {
        return FRenta;
    }
    /**
     *
     * @param FRenta
     */
    public void setFRenta(Date FRenta) {
        this.FRenta = FRenta;
    }

    /**
     *
     * @return
     */
    public Date getFEntrega() {
        return FEntrega;
    }

    /**
     *
     * @param FEntrega
     */
    public void setFEntrega(Date FEntrega) {
        this.FEntrega = FEntrega;
    }
    //Diferencias entre dos fechas
    //@param fechaInicial La fecha de inicio
    //@param fechaFinal  La fecha de fin
    //@return Retorna el numero de dias entre dos fechas
    public static synchronized int diferenciasDeFechas(Date ini, Date fin) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fechaInicioString = df.format(ini);
        try {
            ini = df.parse(fechaInicioString);
        } catch (ParseException ex) {
        }

        String fechaFinalString = df.format(fin);
        try {
            fin = df.parse(fechaFinalString);
        } catch (ParseException ex) {
        }
    if(ini.getTime()<fin.getTime()){
        long fechaInicialMs = ini.getTime();
        long fechaFinalMs = fin.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias= Math.floor(diferencia / (1000 * 60 * 60 * 24));
        return (int)dias;
    } else{
     return 0;
    }
}

    /**
     *
     * @return
     */
    @Override
    public float CalRenta(){
//        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
        return getPrecio()*diferenciasDeFechas(getFEntrega(),getFRenta());
//                *dias;
//        *(FRenta-FEntrega);
    }
}
