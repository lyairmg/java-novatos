/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;

/**
 *
 * @author Yair
 */
public class Autobus extends Vehiculo {
    private float KmPrevio;
    private float KmDespues;
    public Autobus(String placas, float precio, float KmPrevio,float KmDespues, boolean estado){
        super(placas, precio, estado);
        this.KmPrevio=KmPrevio;
        this.KmDespues=KmDespues;
    }

    /**
     * @return the KmPrevio
     */
    public float getKmPrevio() {
        return KmPrevio;
    }

    /**
     * @param KmPrevio the KmPrevio to set
     */
    public void setKmPrevio(float KmPrevio) {
        this.KmPrevio = KmPrevio;
    }

    /**
     * @return the KmDespues
     */
    public float getKmDespues() {
            return KmDespues;
    }

    /**
     * @param KmDespues the KmDespues to set
     */
    public void setKmDespues(float KmDespues) {
        this.KmDespues = KmDespues;
    }
    @Override
    public float CalRenta(){
        return getPrecio()*(getKmDespues()-getKmPrevio());
    }
}
