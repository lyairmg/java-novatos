/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;

/**
 *
 * @author Yair
 */
public abstract class Vehiculo {
    private String placas;
    private float precio;
    private boolean estado;
    public Vehiculo(String placas, float precio, boolean estado){
        this.placas=placas;
        this.precio=precio;
        this.estado=estado;
        
}

    public String getPlacas() {
        return placas;
    }

    public void setPlacas(String placas) {
        this.placas = placas;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    public abstract float CalRenta();
}
