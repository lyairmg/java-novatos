package entidad.x;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener
{

    private Button btn1;

    /** Called when the activity is first created.
     * @param savedInstanceState */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //new code
        btn1 = (Button) findViewById( R.id.btn1 );
        btn1.setOnClickListener(this);
        //
    }

    public void onClick(View view) {
        Toast toast = Toast.makeText(getBaseContext(),"Hola Mundo desde Netbeans",Toast.LENGTH_SHORT); 
        toast.setGravity(Gravity.CENTER, 0, 0);
        LinearLayout toastLayout = (LinearLayout) toast.getView();
        TextView textview = (TextView) toastLayout.getChildAt(0);
        textview.setTextSize(50);
        toast.show();
    }
}