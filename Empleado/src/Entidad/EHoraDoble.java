/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;

/**
 *
 * @author Yair
 */
public class EHoraDoble extends Empleado{
    private int hextras;
    public EHoraDoble(int htrabajadas, float sueldoph){
        super(htrabajadas, sueldoph);
    }

    public int getHextras() {
        return getHtrabajadas()-40;
    }

    public void setHextras(int hextras) {
        this.hextras=hextras;
    }
    @Override
    public double CalSalario() {
        return getSueldoph()*(40+(getHextras()*2));
    }
}