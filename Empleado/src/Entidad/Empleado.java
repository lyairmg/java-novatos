/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;

/**
 *
 * @author Yair
 */
public abstract class Empleado {
    private int htrabajadas;
    private float sueldoph;
    
    public Empleado(int htrabajadas, float sueldoph){
        this.htrabajadas=htrabajadas;
        this.sueldoph=sueldoph;
    }
    /**
     * @return the htrabajadas
     */
    public int getHtrabajadas() {
        return htrabajadas;
    }
    /**
     * @param htrabajadas the htrabajadas to set
     */
    public void setHtrabajadas(int htrabajadas) {
        this.htrabajadas = htrabajadas;
    }
    /**
     * @return the sueldoph
     */
    public float getSueldoph() {
        return sueldoph;
    }
    /**
     * @param sueldoph the sueldoph to set
     */
    public void setSueldoph(float sueldoph) {
        this.sueldoph = sueldoph;
    }
    public abstract double CalSalario();
}
