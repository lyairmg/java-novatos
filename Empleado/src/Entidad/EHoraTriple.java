/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;
/**
 *
 * @author Yair
 */
public class EHoraTriple extends Empleado{
    private int hextras;
    public EHoraTriple(int htrabajadas, float sueldoph){
        super(htrabajadas, sueldoph);
    }
    /**
     * @return the hextras
     */
    public int getHextras() {
        return getHtrabajadas()-40;
    }
    /**
     * @param hextras the hextras to set
     */
    public void setHextras(int hextras) {
        this.hextras=hextras;
    }
    @Override
    public double CalSalario() {
        int i;
        return getSueldoph()*(50+((getHextras()-5)*3));
    }
    /* Metodo para comprobar salida de datos mediante consola inutil para un from
    public String toString(){
        return "Sueldo "+sueldoph+" Horas "+htrabajadas+" Extras 1 "+hextras+" Extras 2 "+getHextras();
    }*/
}
