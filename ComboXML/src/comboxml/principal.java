package comboxml;

import java.awt.Font;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class principal extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	Combox_provi combo;
	private JLabel jLabel = null;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(() -> {
                    try {
                        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                    } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
                    }
                    principal thisClass = new principal();
                    thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    thisClass.setVisible(true);
                });
	}

	/**
	 * This is the default constructor
	 */
	public principal() {
		super();
		initialize();
		
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(575, 200);
		this.setContentPane(getJContentPane());
		this.setLocationRelativeTo(null);
		this.setTitle("CARGAR JCOMBOBOX DESDE UN ARCHIVO XML-----JAVAFACE");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabel = new JLabel();
			jLabel.setBounds(new Rectangle(5, 3, 145, 154));
			jLabel.setIcon(new ImageIcon(getClass().getResource("/principal/imagenes/winodws_7_uac.png")));
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			/********************************/
			combo=new Combox_provi();
			combo.setBounds(new Rectangle(157, 5, 397, 151));
			combo.setFont(new Font("SansSerif", Font.PLAIN, 24));
			jContentPane.add(combo, null);
			jContentPane.add(jLabel, null);
		}
		return jContentPane;
	}
	

	
}  //  @jve:decl-index=0:visual-constraint="153,68"
