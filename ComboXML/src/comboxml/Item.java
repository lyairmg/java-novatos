package comboxml;

public class Item {

    private String item = "";
    private Object ItemData = null;

    public Item() {

    }

    public Object getItemData() {
        return ItemData;
    }

    public void setItemData(Object ItemData) {
        this.ItemData = ItemData;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @Override
    public String toString(){
        return getItem();
    }
    
}
