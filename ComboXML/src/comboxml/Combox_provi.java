package comboxml;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

@SuppressWarnings("rawtypes")
public class Combox_provi extends JComboBox{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1805062591039152967L;

	public Combox_provi() {
        inicializar();
    }

    private void inicializar() {
        cargarCiudades();
    }

    public void cargarCiudades() {
        List<Item> items = new ArrayList<Item>();
        String strRutaFileCiudades = "";
        try {
            strRutaFileCiudades = getClass().getResource("/xml/nombres.xml").toURI().getPath();
        } catch (URISyntaxException ex) {
            System.out.println("No se encontro la ruta del fichero xml");
        }
        String[] params = new String[]{"persona", "codigo", "nombre"};
        try {
            items = cargador.cargarDatosDesdeXml(strRutaFileCiudades, params);
        } catch (Exception ex) {
            System.out.println("No se pudo cargar el fichero xml");
        }

        DefaultComboBoxModel model = (DefaultComboBoxModel) this.getModel();

        for (Item item : items){
            model.addElement(item);
        }
    }
}
