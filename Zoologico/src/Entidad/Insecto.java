/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;

/**
 *
 * @author Yair
 */
public class Insecto extends Animal{
    private boolean volar;

    public Insecto(boolean volar, String especie, String nombre, float peso, int jaula) {
        super(especie, nombre, peso, jaula);
        this.volar = volar;
    }

    public boolean isVolar() {
        return volar;
    }

    public void setVolar(boolean volar) {
        this.volar = volar;
    }

    @Override
    public void QueAnimal(){}
}
