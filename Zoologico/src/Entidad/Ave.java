/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;

/**
 *
 * @author Yair
 */
public class Ave extends Animal{
    private String colorP;

    public Ave(String colorP, String especie, String nombre, float peso, int jaula) {
        super(especie, nombre, peso, jaula);
        this.colorP = colorP;
    }

    public String getColorP() {
        return colorP;
    }

    public void setColorP(String colorP) {
        this.colorP = colorP;
    }

    @Override
    public void QueAnimal() {}
}
