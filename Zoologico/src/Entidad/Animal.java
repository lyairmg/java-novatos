/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidad;

/**
 *
 * @author Yair
 */
public abstract class Animal {
    private String especie;
    private String nombre;
    private float peso;
    private int jaula;

    public Animal(String especie, String nombre, float peso, int jaula) {
        this.especie = especie;
        this.nombre = nombre;
        this.peso = peso;
        this.jaula = jaula;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public int getJaula() {
        return jaula;
    }

    public void setJaula(int jaula) {
        this.jaula = jaula;
    }
    
    public abstract void QueAnimal();
}
